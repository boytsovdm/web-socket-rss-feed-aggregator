Web Socket RSS Feed Aggregator
============================

INSTALLATION
------------

### Install via Composer

If you do not have [Composer](http://getcomposer.org/), you may install it by following the instructions
at [getcomposer.org](http://getcomposer.org/doc/00-intro.md#installation-nix) and configure it, so composer command become available.

You can then install this project using the following command:

~~~
cd {project_root}
composer global require "fxp/composer-asset-plugin:^1.3.1"
composer install
~~~

### Database connection
Create database "xdewwd_feeds".
Insert your database credentials into db.sample.php and rename it to db.php.
Run
~~~
cd {project_root}
php yii migrate
~~~

### Running WebSocket server
~~~
cd {project_root}
php yii ws
~~~

Now you can go to Feeds main page!

### Adding new RSS feeds
Feeds can be added by editing FEED_SOURCE_URLS array in
~~~
models/FeedSource.php
~~~
