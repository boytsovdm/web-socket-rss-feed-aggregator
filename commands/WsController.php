<?php

namespace app\commands;

use app\components\FeedsWsServer;

use yii\console\Controller;

use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;

class WsController extends Controller
{

	public function actionIndex() {
		$server = IoServer::factory( new HttpServer( new WsServer( new FeedsWsServer() ) ), 8080 );
		$server->run();
	}

}
