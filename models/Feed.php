<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

use Yii;

/**
 * This is the model class for table "feed".
 *
 * @property integer $id
 * @property integer $feedSourceId
 * @property string $guid
 * @property string $title
 * @property string $link
 * @property string $description
 * @property string $pubDate
 * @property string $enclosureUrl
 * @property string $feedSourceImageUrl
 * @property boolean $isNew
 * @property boolean $isViewed
 * @property boolean $isHidden
 *
 */
class Feed extends ActiveRecord
{

	public function __call($name, $args)
	{
		return 'Unknown method';
	}

	public static function __callStatic($name, $args)
	{
		return 'Unknown method';
	}

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'feed';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['id', 'feedSourceId'], 'integer'],
			[['guid', 'title', 'description'], 'string'],
			[['link', 'enclosureUrl', 'feedSourceImageUrl'], 'url', 'skipOnEmpty' => true],
			[['pubDate'], 'datetime', 'format' => 'php:Y-m-d H:i:s'],
			[['isNew', 'isViewed', 'isHidden'], 'boolean'],
		];
	}

	public static function getAll($params)
	{
		$feeds = self::find()
			->where(['isHidden' => false])
			->limit(10);
		if ( isset($params->show) and $params->show === 'new' ) {
			$feeds->andWhere(['isViewed' => false]);
		}

		if ( isset($params->page) ) {
			$feeds->offset($params->page * 10);
		}

		if ( isset($params->sort) and $params->sort === 'title' ) {
			$feeds->orderBy('title');
		} else {
			$feeds->orderBy(['pubDate' => SORT_DESC]);
		}

		return ArrayHelper::toArray($feeds->all());
	}

	public static function create($item, $feedSource)
	{
		$feed = Feed::findOne(['guid' => $item->guid]);
		if ( !$feed ) {
			$feed = new Feed();
		}

		$feed->feedSourceId = $feedSource->id;
		$feed->guid = $item->guid;
		$feed->title = $item->title;
		$feed->link = $item->link;
		$feed->description = trim($item->description);
		$pubDate = new \DateTime($item->pubDate);
		$feed->pubDate = $pubDate->format('Y-m-d H:i:s');
		$feed->enclosureUrl = isset($item->enclosure->{'@attributes'}->url)
			? $item->enclosure->{'@attributes'}->url
			: null;
		$feed->feedSourceImageUrl = $feedSource->imageUrl;
		$feed->isNew = true;

		$feed->save();
	}

	public static function makeOld($params)
	{
		if ( isset($params->id) ) {
			$feed = self::findOne($params->id);
			$feed->isNew = false;
			$feed->save();
		}
	}

	public static function makeViewed($params)
	{
		if ( isset($params->id) ) {
			$feed = self::findOne($params->id);
			$feed->isViewed = true;
			$feed->save();
		}
	}

	public static function hide($params)
	{
		if ( isset($params->id) ) {
			$feed = self::findOne($params->id);
			$feed->isHidden = true;
			$feed->save();
		}
	}

	public static function email($params)
	{
		if ( isset($params->id) and isset($params->email) ) {
			$feed = self::findOne($params->id);
			Yii::$app->mailer->compose()
				->setTo($params->email)
				->setFrom(['boytsov.dm@gmail.com' => 'Dmitry Boytsov'])
				->setSubject('Feeds - news')
				->setHtmlBody($feed->description . "\n" . $feed->link)
				->send();
		}
	}

}