<?php

namespace app\models;

use yii\db\ActiveRecord;

use Yii;

/**
 * This is the model class for table "feed_source".
 *
 * @property integer $id
 * @property string $title
 * @property string $url
 * @property string $imageUrl
 * @property string $lastBuildDate
 */
class FeedSource extends ActiveRecord
{

	const FEED_SOURCE_URLS = [
		'https://lenta.ru/rss',
		// 'https://news.yandex.ru/index.rss',
		// 'https://news.mail.ru/rss/91/'
	];

	public function __call($name, $args)
	{
		return 'Unknown method';
	}

	public static function __callStatic($name, $args)
	{
		return 'Unknown method';
	}

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'feed_source';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['id'], 'integer'],
			[['title', 'lastBuildDate'], 'string'],
			[['url', 'imageUrl'], 'url'],
			[['lastBuildDate'], 'datetime', 'format' => 'php:Y-m-d H:i:s']
		];
	}

	public function getFeeds()
	{
		return $this->hasMany(Feed::class, ['feedSourceId' => 'id'])->all();
	}

	public static function getAll()
	{
		return self::find()->asArray()->all();
	}

	public static function getImageUrl($id)
	{
		$feedSource = self::findOne($id);
		return $feedSource->imageUrl;
	}

	public static function updateAllFeedSources()
	{
		foreach (self::FEED_SOURCE_URLS as $url) {
			$feedSource = FeedSource::findOne(['url' => $url]);
			if ( $feedSource ) {
				$lastBuildTs = strtotime($feedSource->lastBuildDate);
				// Updated more than 10 mins ago
				if ( (time() - $lastBuildTs) < 600 ) {
					continue;
				}
			}

			$xml = new \SimpleXMLElement($url, null, true);
			$rss = simplexml_load_string($xml->asXML(), 'SimpleXMLElement', LIBXML_NOCDATA);
			$channel = json_decode(json_encode($rss->children()->channel));

			if ( !$feedSource ) {
				$feedSource = new FeedSource();
			}

			echo "Updating feed {$channel->title} \n";

			$feedSource->title = $channel->title;
			$feedSource->url = $url;
			$feedSource->imageUrl = $channel->image->url;
			$lastBuildDate = new \DateTime();
			$feedSource->lastBuildDate = $lastBuildDate->format('Y-m-d H:i:s');

			$feedSource->save();

			foreach ($channel->item as $item) {
				Feed::create($item, $feedSource);
			}
		}
	}

}