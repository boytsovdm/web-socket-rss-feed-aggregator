<?php

namespace app\assets;

use yii\web\AssetBundle;
use yii\web\View;

class BackboneAsset extends AssetBundle
{
	public $sourcePath = '@vendor/bower';

	public $jsOptions = array(
		'position' => \yii\web\View::POS_HEAD
	);

	public $depends = [
		\yii\web\JqueryAsset::class
	];

	public function init()
	{
		parent::init();
		$this->js[] = YII_DEBUG ? 'underscore/underscore.js' : 'underscore/underscore-min.js';
		$this->js[] = YII_DEBUG ? 'backbone/backbone.js' : 'backbone/backbone-min.js';
	}

}