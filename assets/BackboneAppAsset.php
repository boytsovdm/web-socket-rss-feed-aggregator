<?php

namespace app\assets;

use yii\web\AssetBundle;
use yii\web\View;

class BackboneAppAsset extends AssetBundle
{

	public $basePath = '@webroot';
	public $baseUrl = '@web';

	public $css = [
		'css/feeds.css',
	];

	public $js = [
		'app/feeds.js',
	];

}