<?php

/* @var $this yii\web\View */

$this->title = 'Feed';
?>

<div id="accordion" class="feed-container">
	<div class="feed-control-container">
		<div class="btn-group" role="group" aria-label="...">
		  <button type="button" class="btn btn-default feed-sort-pubDate" disabled>Sort By Date</button>
		  <button type="button" class="btn btn-default feed-sort-title">Sort By Title</button>
		  <button type="button" class="btn btn-default feed-show-all" disabled>Show All</button>
		  <button type="button" class="btn btn-default feed-show-new">Only Show Unread</button>
		</div>
	</div>
	<div class="panel-group"></div>
	<div class="feed-control-container">
		<div class="btn-group" role="group" aria-label="...">
		  <button type="button" class="btn btn-default feed-prev-page">Previos</button>
		  <button type="button" class="btn btn-default feed-next-page">Next</button>
		</div>
	</div>
</div>

<script id="feedTpl" type="text/template">
	<div class="panel-heading">
		<h4 class="panel-title">
			<a class="accordion-toggle" data-toggle="collapse" data-id="<%= id %>" data-parent="#accordion" href="#collapse<%= id %>">
				<img class="feed-source-image" src="<%= feedSourceImageUrl %>"><%= title %>
			</a>
			<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
			<span class="glyphicon glyphicon-envelope" aria-hidden="true"></span>
		</h4>
	</div>
	<div id="collapse<%= id %>" class="panel-collapse collapse">
		<div class="panel-body">
			<p>
				<%= pubDate %>
			</p>
			<p>
				<img src="<%= enclosureUrl %>">
			</p>
				<%= description %>
			<p>
				<a href="<%= link %>" target="_blank">Читать</a>
			</p>
		</div>
	</div>
</script>