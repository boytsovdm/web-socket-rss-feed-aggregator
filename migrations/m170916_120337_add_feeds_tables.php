<?php

use yii\db\Migration;

class m170916_120337_add_feeds_tables extends Migration
{
	public function safeUp()
	{
		$this->createTable('feed_source', [
			'id' => $this->primaryKey(),
			'title' => $this->string()->notNull(),
			'url' => $this->string()->notNull(),
			'imageUrl' => $this->string()->notNull(),
			'lastBuildDate' => $this->dateTime()->notNull(),
		]);

		$this->createTable('feed', [
			'id' => $this->primaryKey(),
			'feedSourceId' => $this->integer()->notNull(),
			'guid' => $this->string()->notNull(),
			'title' => $this->string()->notNull(),
			'link' => $this->string()->notNull(),
			'description' => $this->text(),
			'pubDate' => $this->string()->notNull(),
			'enclosureUrl' => $this->string(),
			'feedSourceImageUrl' => $this->string(),
			'isNew' => $this->boolean()->notNull()->defaultValue(false),
			'isViewed' => $this->boolean()->notNull()->defaultValue(false),
			'isHidden' => $this->boolean()->notNull()->defaultValue(false),
		]);

		$this->addForeignKey('feed_fk', '{{%feed}}', 'feedSourceId', '{{%feed_source}}', 'id', 'CASCADE');
	}

	public function safeDown()
	{
		$this->dropForeignKey('feed_fk', '{{%feed}}');
		$this->dropTable('feed_source');
		$this->dropTable('feed');
	}

}
