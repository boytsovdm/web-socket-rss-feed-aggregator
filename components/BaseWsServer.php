<?php

namespace app\components;

use Yii;

use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;

class BaseWsServer implements MessageComponentInterface {

	protected $clients;

	public function __construct() {
		$this->clients = new \SplObjectStorage;
	}

	public function onOpen(ConnectionInterface $conn) {
		$this->clients->attach($conn, $conn->resourceId);
		echo "New connection ({$conn->resourceId})\n";
	}

	public function onMessage(ConnectionInterface $from, $message) {
		echo "{$from->resourceId} said: {$message}\n";

		foreach ($this->clients as $client) {
			$client->send("You said: {$message}");
		}
	}

	public function onClose(ConnectionInterface $conn) {
		$this->clients->detach($conn);
		echo "Client {$conn->resourceId} disconnected\n";
	}

	public function onError(ConnectionInterface $conn, \Exception $e) {
		echo "Error: {$e->getMessage()}\n";
		$conn->close();
	}

	protected function sendAll($message)
	{
		foreach ($this->clients as $client) {
			$client->send($message);
		}
	}

	protected function sendOnRequest($requester, $message)
	{
		foreach ($this->clients as $client) {
			if ( $requester === $client ) {
				$client->send($message);
			}
		}
	}

}