<?php

namespace app\components;

use Yii;

use Ratchet\ConnectionInterface;

class FeedsWsServer extends BaseWsServer {

	public function onMessage(ConnectionInterface $from, $request) {
		echo "{$from->resourceId} made request: {$request}\n";

		$request = json_decode($request);
		list($class, $method) = explode('/', $request->command);
		$class = "app\\models\\{$class}";
		$data = call_user_func_array([$class, $method], $request->params);

		$this->sendOnRequest($from, json_encode($data) );
	}

}