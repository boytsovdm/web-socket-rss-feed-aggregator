(function($) {

	var wsUrl = 'ws://localhost:8080';
	var ws;

	wsInit = new WebSocket(wsUrl);
	wsInit.onopen = function() {
		wsInit.send(JSON.stringify({command: 'FeedSource/updateAllFeedSources', params: []}));
	};

	Backbone.sync = function(method, model, options) {
		ws = new WebSocket(wsUrl);

		ws.onopen = function() {
			ws.send(JSON.stringify({command: model.url, params: [model.data]}));
		};

		ws.onmessage = function(message) {
			feedsUpdatedOnServer = true;
			options.success(JSON.parse(message.data));
		};
	};

	var FeedItem = Backbone.Model.extend({
	});

	var Feed = Backbone.Collection.extend({
		url: 'Feed/getAll',
		model: FeedItem
	});

	var FeedItemView = Backbone.View.extend({
		tagName: 'div',
		className: 'panel panel-default',
		template: $('#feedTpl').html(),

		events: {
			'click .accordion-toggle': function() {
				this.model.url = 'Feed/makeViewed';
				this.model.data = {id: this.model.id};
				this.model.save();
				this.$el.removeClass('feed-new');
			},
			'click .glyphicon-remove': function(e) {
				this.model.url = 'Feed/hide';
				this.model.data = {id: this.model.id};
				this.model.save();
				this.$el.slideUp();
			},
			'click .glyphicon-envelope': function() {
				var email = prompt('Please enter your email');
				this.model.url = 'Feed/email';
				this.model.data = {id: this.model.id, email: email};
				this.model.save();
			},
		},

		render: function() {
			var tmpl = _.template(this.template);
			if ( this.model.get('isViewed') === 0) {
				this.$el.addClass('feed-new');
			}
			this.$el.html(tmpl(this.model.toJSON()));
			return this;
		}
	});

	var FeedView = Backbone.View.extend({
		el: $('.feed-container'),
		page: 0,
		sort: 'pubDate',
		show: 'all',

		initialize: function() {
			this.pagingControl();
			this.collection = new Feed();
			this.fetch();
		},

		fetch: function() {
			this.collection.bind('reset', _.bind(this.render, this));
			this.collection.data = {page: this.page, sort: this.sort, show: this.show};
			this.collection.fetch({reset: true});
		},

		events: {
			'click .feed-sort-pubDate': function() {
				this.sort = 'pubDate';
				this.$el.find('.feed-sort-pubDate').prop('disabled', true);
				this.$el.find('.feed-sort-title').prop('disabled', false);

				this.fetch();
			},
			'click .feed-sort-title': function() {
				this.sort = 'title';
				this.$el.find('.feed-sort-title').prop('disabled', true);
				this.$el.find('.feed-sort-pubDate').prop('disabled', false);

				this.fetch();
			},
			'click .feed-show-all': function() {
				this.show = 'all';
				this.$el.find('.feed-show-all').prop('disabled', true);
				this.$el.find('.feed-show-new').prop('disabled', false);

				this.fetch();
			},
			'click .feed-show-new': function() {
				this.show = 'new';
				this.$el.find('.feed-show-new').prop('disabled', true);
				this.$el.find('.feed-show-all').prop('disabled', false);

				this.fetch();
			},
			'click .feed-prev-page': function() {
				this.page--;
				this.pagingControl();
				this.fetch();
			},
			'click .feed-next-page': function() {
				this.page++;
				this.pagingControl();
				this.fetch();
			},
		},

		pagingControl: function() {
			if ( this.page === 0 ) {
				this.$el.find('.feed-prev-page').prop('disabled', true);
			} else {
				this.$el.find('.feed-prev-page').prop('disabled', false);
			}
		},

		render: function() {
			var that = this;
			this.$el.find('.panel-group').html('');
			_.each(this.collection.models, function(item) {
				that.renderFeedItem(item);
			}, this);
		},

		renderFeedItem: function(item) {
			var feedItemView = new FeedItemView({
				model: item
			});
			this.$el.find('.panel-group').append(feedItemView.render().el);
		}
	});

	var feedView = new FeedView();

} (jQuery));