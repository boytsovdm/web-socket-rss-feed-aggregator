<?php

namespace app\controllers;

use app\models\FeedSource;
use app\models\Feed;

use yii\web\Controller;
use yii\web\Response;

use Yii;

class MainController extends Controller
{

	public $layout = 'feeds';

	public function actionIndex()
	{
		return $this->render('index');
	}

	public function actionAdd()
	{
		foreach (FeedSource::FEED_SOURCE_URLS as $feedSourceUrl) {
			$this->parseFeedSource($feedSourceUrl);
		}
	}

	protected function parseFeedSource($url)
	{
		$xml = new \SimpleXMLElement($url, null, true);
		$rss = simplexml_load_string($xml->asXML(), 'SimpleXMLElement', LIBXML_NOCDATA);
		$channel = json_decode(json_encode($rss->children()->channel));

		$feedSource = FeedSource::findOne(['url' => $url]);

		if ( !$feedSource ) {
			$feedSource = new FeedSource();
		}

		$feedSource->title = $channel->title;
		$feedSource->url = $url;
		$feedSource->imageUrl = $channel->image->url;
		$lastBuildDate = isset($channel->lastBuildDate)
			? $lastBuildDate = new \DateTime($channel->lastBuildDate)
			: $lastBuildDate = new \DateTime($channel->item[0]->pubDate);
		$feedSource->lastBuildDate = $lastBuildDate->format('Y-m-d H:i:s');

		$feedSource->save();

		foreach ($channel->item as $item) {
			$feed = Feed::findOne(['guid' => $item->guid]);
			if ( !$feed ) {
				$feed = new Feed();
			}

			$feed->feedSourceId = $feedSource->id;
			$feed->guid = $item->guid;
			$feed->title = $item->title;
			$feed->link = $item->link;
			$feed->description = trim($item->description);
			$pubDate = new \DateTime($item->pubDate);
			$feed->pubDate = $pubDate->format('Y-m-d H:i:s');
			$feed->enclosureUrl = isset($item->enclosure->{'@attributes'}->url)
				? $item->enclosure->{'@attributes'}->url
				: null;

			$feed->save();
		}

		$this->redirect('/');
	}

}
